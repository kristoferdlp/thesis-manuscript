\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Table of Contents}{iv}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Tables}{v}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Figures}{vi}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abstract}{vii}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{viii}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Review of Related Literature}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}A Brief Review of Malaria}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Plasmodium Species}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Plasmodium Lifecycle}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Malaria Symptoms, Diagnosis, and Treatment}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}State of the Art Intelligent Systems for Malaria}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Shape Descriptors}{17}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Deep Convolutional Neural Networks}{21}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Problem Statement}{24}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Methodology and Results}{26}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Dataset}{26}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Data Preprocessing}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Image Segmentation}{28}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Data Augmentation}{28}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Feature Extraction}{29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Identification of \textit {P. falciparum} gametocytes}{30}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.7}Detection and Identification of Malaria Species}{32}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusion and Recommendation}{40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Conclusion}{40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Summary of Contributions}{41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Future Research}{41}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Hardware Specifications Used}{43}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Software Requirements}{44}
