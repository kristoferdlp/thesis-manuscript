\chapter{Review of Related Literature} \label{sec:back}

We initially review malaria, its causes, symptoms, diagnosis and treatment and the continuing challenges with conventional diagnosis. We then present the promising results of some intelligent systems built to automate the diagnosis of malaria. Lastly, a survey of shape features and a discussion of convolutional neural networks are presented to give background to the methodology presented in the next chapter.
\section{A Brief Review of Malaria}
This section gives an overview of malaria - cause, symptoms, diagnosis and treatment.
%discuss malaria species, lifecycle
\subsection{Plasmodium Species} \label{sec:species}
Malaria parasites are protozoans belonging to the genus \textit{Plasmodium}. This genus is further divided into two subgenera: \textit{Plasmodium} and \textit{Laverania}. \textit{Plasmodium vivax}, \textit{Plasmodium malariae}, and \textit{Plasmodium ovale} all belong to the subgenus \textit{Plasmodium}, while \textit{Plasmodium falciparum} is classified under the subgenus \textit{Laverania} due to its significant differences from the other three species \cite{Paniker}.

\textit{Plasmodium vivax} is the cause of most malaria cases, accounting for around 80\% of the total infections in Asia and America. This species causes benign tertian malaria with frequent lapses \cite{Paniker}.

The parasitemia of \textit{P. vivax} is usually not heavy. In blood samples, infected erythrocytes would contain only one trophozoite, and with less than 5\% of the cells being infected. In its trophozoite stage, the structure of the parasite is well-defined, having a prominent central vacuole. The cytoplasm is distinguishably blue, while the nucleus red in stained films. An infected erythrocyte would be slightly enlarged and exhibits red granulations called \textit{Schuffner's dots} \cite{Paniker}.

Schizonts usually appear in about 36-40 hours, maturing after the next 6-8 hours. During the gametocyte stage, the infected red blood cell is nearly filled. The macrogametocyte stains deep blue with a compact nucleus. Pigment granules also become more prominent \cite{Paniker}. Figure \ref{vivax} shows the erythocytic schizogony of \textit{P. vivax}.

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=5in]{vivax.png}
	\caption{\textit{Plasmodium vivax}}
	\caption*{\textit{Source:} Paniker's Textbook of Medical Parasitology 7th ed., p.70 \cite{Paniker}}
	\label{vivax}
	\end{center}
\end{figure}

\textit{Plasmodium falciparum} is most distinguishable for its sickle-shaped gametocyte. This species is the most pathogenic of all species, posing high rate of complications and fatality if left untreated \cite{Paniker}.

The trophozoites of this species has very delicate and appear as tiny ring-forms usually attached near the walls of the erythrocytes. Many of the ring-forms have binucleate rings (double chromatin dots) \cite{Paniker}.

\textit{P. falciparum} schizonts are the smallest. Unlike in \textit{P. vivax}, the infected red blood cell does not appear enlarged but show a few brick-red dots called \textit{Maurer's clefts} \cite{Paniker}. Figure \ref{falciparum} shows the erythocytic schizogony of \textit{P. falciparum}.

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=5in]{falciparum-paniker.png}
	\caption{\textit{Plasmodium falciparum}}
	\caption*{\textit{Source:} Paniker's Textbook of Medical Parasitology 7th ed., p.72 \cite{Paniker}}
	\label{falciparum}
	\end{center}
\end{figure}

\textit{Plasmodium malariae} and \textit{Plasmodium ovale} are other species of malaria parasite. These two species account for a very small percentage of malaria cases in Africa, Sri Lanka, Burma, and India \cite{Paniker}.

\subsection{Plasmodium Lifecycle} \label{sec:lifecycle}
The life cycle of a malaria parasite involves two hosts: a female \textit{Anopheles} mosquito and an intermediate host (human). This cycle has two phases: an asexual phase and a sexual phase. The asexual phase occurs in humans and it is in this stage that the parasite multiplies by cell division called \textit{schizogony}. The splitting process occurs in two locations in a human host. First is in the liver cells, while the second is inside erythrocytes or red blood cells. Schizogony in the liver is essential for the sporozoites to mature before infecting the erythrocytes\cite{Paniker}. Figure \ref{lifecycle} shows the life cycle of \textit{Plasmodium} parasites.
\begin{figure}[h]
	\begin{center}
	\includegraphics[width=5in]{lifecycle2.png}
	\caption{Life cycle of \textit{Plasmodium}}
	\caption*{\textit{Source:} Markell and Voge's Medical Parasitology 9th ed., p.80 \cite{Markell}}
	\label{lifecycle}
	\end{center}
\end{figure}

\subsection{Malaria Symptoms, Diagnosis, and Treatment}  \label{sec:diagnosis}
Malaria is often characterized by recurring fever. Depending on the infecting species, the fever could happen every 24 (quotidian malaria), 48 (tertian malaria), or 72 (quartan malaria) hours. Tertian malaria is caused by either \textit{P. vivax} or \textit{P. falciparum}, while quartan malaria is caused by \textit{P. malariae}. The fever cycle, however, is not enough indication of infecting species since multiple parasite groups could undergo schizogony alternately, each group having its own cycle \cite{Markell}.

Aside from fever, a malaria-infected person exhibits the following symptoms:
\begin{itemize}
	\item chills
	\item sweating
	\item headaches
	\item nausea and vomiting
	\item body aches
\end{itemize}
Further physical findings may include increased respiratory rate, enlarged spleen, mild jaundice, and enlargement of the liver \cite{CDC}.

Pathological changes in the organs also manifest in malaria-infected hosts. The liver becomes enlarged and congested. The spleen, on the other hand, can be affected in two different ways. In acute infection, it becomes soft and moderately enlarged and congested. The spleen becomes hard hard and turns dark in chronic cases. Kidneys also become enlarged, and anemia starts to develop. The brain could also become congested as infected red blood cells reach and clog the capillaries of the brain, possibly inducing hemorrhage \cite{Paniker}.

The diagnosis of malaria is done through demonstration of malaria parasites in the blood. Two types of smears are usually prepared: a \textit{thin} smear and a \textit{thick} smear \cite{Paniker}.

A thin smear is used in determining the infecting species. This type of smear is prepared from capillary blood drawn from a fingertip, applied to a slide, and stained by one of the Romanowsky stains such as \textit{Giemsa} to highlight the parasite \cite{Paniker}. Figure \ref{thinsmear} shows an example of a thin smear.

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=5in]{thinsmear.jpg}
	\caption{A thin smear}
	\label{thinsmear}
	\end{center}
\end{figure}

Thick smear, on the other hand, would require around two to four times the amount of blood needed to prepare a thin smear. Aside from staining, a thick smear undergoes \textit{dehemoglobuliniation}. A thick smear is more suitable for rapid detection of infection because it concentrates a larger number of red blood cells in a small area, but species identification is difficult in thick smears even for trained microscopists \cite{Paniker}. Figure \ref{thicksmear} shows an example of a thick smear.

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=5in]{thicksmear.jpg}
	\caption{A thick smear}
	\label{thicksmear}
	\end{center}
\end{figure}

In blood smears, both thick and thin, ring forms of all Plasmodium species appear with a distinguishable blue cytoplasm and detached nuclear dot(s). Ring forms of \textit{P. vivax}, \textit{P. ovale}, and \textit{P. malariae} appear compact, while those of \textit{P. falciparum} appear fine and with double chromatin \cite{Paniker}.

Oval-shaped gametocytes are seen in \textit{P. vivax}, \textit{P. ovale}, and \textit{P. malariae}, while \textit{P. falciparum} appear crescent-shaped \cite{Paniker}. Enlarged red blood cells with \textit{Schuffner's dots} are characteristic of \textit{P. vivax}, while in \textit{P. falciparum}, cells are normal in size but with \textit{Maurer's dots} and basophilic stippling \cite{Paniker}.

Treating malaria requires administration of anti-malarial drugs. These drugs are used to prevent relapse, to prevent transmission, and to initiate prophylaxis. The most common anti-malarial drugs are chloroquine, amodiaquin, quinine, pyrimethamine, doxycycline, and artemesinin \cite{Paniker}.

The infecting species of malaria parasite play a significant role on the course of treatment. Another factor considered is the resistance of parasites to certain drugs \cite{Paniker}.

\textit{P. vivax}, \textit{P. ovale}, and \textit{P. malariae} are usually treated similarly. Chloroquine is administered to the patient over a period of three days. To prevent relaspe, primaquine is also given to the patient for fourteen days \cite{Paniker}.

Treatment for \textit{P. falciparum} is different. Chloroquine is also administered at the same dosage, but primaquine is only given once to prevent relapse. In cases of chloroquine resistance, artemesinin is given instead \cite{Paniker}.

The formulation of a vaccine for malaria is currently an active area of research. Multiple vaccines are currently under development and a few pose good potential \cite{Paniker}.
\section{State of the Art Intelligent Systems for Malaria} \label{sec:StateoftheArt}

Intelligent systems for malaria have been developed with aims to automate the diagnosis and classification of blood slides, and to counter-check, and even minimize human error. To do this, images of the slides viewed from a microscope needs to be taken and used as input to these systems.

Various techniques in digital image processing and computer vision were employed in scientific research focused on malaria detection and classification. One main step in detecting the presence of malaria parasites in an image of a blood sample is to segment possible regions of interest (foreground) from the background. The background for the malaria images is a large region of stained blood plasma, and depending on the quality of staining performed and the image captured, the color of the background varies from a bright pink to a dull gray tone. Figure \ref{malariatones} shows very different background colors between two image samples.
\begin{figure}
\centering
\includegraphics[width=5in]{malariatone1.jpg}
\includegraphics[width=5in]{malariatone2.jpg}
\caption{Difference in background color of two images of blood smears}
\label{malariatones}
\end{figure}
%review papers here
%segmentation
One research focused on segmentation is done by Makkapati and Rao \cite{Makkapati-Rao}. The research addressed the problems due to variability and presence of artifacts in microscope images of blood samples. The authors used a segmentation scheme based on the HSV color space to separate the red blood cells (RBC) and parasites. This scheme focuses on detecting the parasites' chromatin within the RBCs once the segmentation was done. The HSV color space was used since the RGB color space is not intuitive for processing unlike HSV, which offers a representation that could be used to easily identify among color families even with varying image qualities. Given 55 test images, the specificity and sensitivity of this approach are 83\% and 98\% \cite{Makkapati-Rao}. Alternative to HSV is the La*b* color space as presented in the work by Nanoti, et. al \cite{Nanoti}. Their work showed that shape and texture features can be effectively extracted in the a* and b* channels.

A similar research was done by Anggraini, et. al \cite{Anggraini}. The proposed algorithm does not consider for segmentation the hue of the individual pixels of the image. Instead, a global threshold was obtained by varying the contrast among pixels, and was used to classify each pixel as belonging to either the foreground or the background. To do this, pre-processing of the images to obtain `uniform' images was done by converting all input images to grayscale. After which, the images were filtered to normalize the pixel intensities around a median value, before proceeding with obtaining the histogram of the individual images, and expanding each until the two intensity classes: foregound and background, becomes distinct given a threshold intensity \cite{Anggraini}.

%morphological approaches
Some research employ more morphological techniques than the two previous ones. For one, the work done by Das, et. al \cite{Das} uses thresholding, marker-controlled watershed algorithm, and Haralick textural feature extraction.
Another work, by Kareem, et. al \cite{Kareem}, also uses morphological image transformations to detect malaria parasites from Giemsa-stained blood films. First, a grayscale image is dilated and eroded to highlight the parasites and platelets (foreground). The cells and parasites are then identified based on a combination of annular ring ratio method, size, and intensity variation.

In conjunction with the techniques explored in the previously mentioned research, machine learning techniques serve as the core for the decision systems for malaria. Some, like Das et. al \cite{Das}, employ generative methods such as multivariate regression models. Anggraini, et. al \cite{Anggraini}, used Bayes decision theory to classify images after segmentation.

Other researchers use discriminative models to classify malaria images. One such research is done by Barros, et. al \cite{Barros}. This particular research used artificial neural networks, alongside Bayesian networks, to construct a model to diagnose asymptomatic malaria. Another is the work by Nanoti, et. al \cite{Nanoti}. Their research utilized a K-nearest neighbor model on shape and texture features after using ANOVA to trim down the number of features to be used in classification. Their set of features include the following:
\begin{itemize}
	\item Gray-level co-occurrence matrix
	\item Gray-level run length matrix
	\item Histogram
	\item Entropy
	\item Fractal Dimension
\end{itemize}

Pinkaew, et. al explored the use of support vector machines in malaria species classification \cite{Pinkaew}. Their work used statistical measurements such as mean intensity, standard deviation, kurtosis, skewness and entropy to characterize regions of interest. Training on 40 \textit{P. falciparum} and 25 \textit{P. vivax} images, the study's SVM model with radial basis kernel gained an accuracy of 85.71\% in identifying \textit{P. falciparum} and 78.72\% in identifying \textit{P. vivax}. A similar research was done by Mohammed, et. al \cite{Mohammed} but they employed template matching and normalized cross-correlation function to discriminate among the four species of \textit{Plasmodium} with an accuracy of 95\%.

Delahunt, et. al \cite{Delahunt} also used SVM, together with ANN to detect \textit{Plasmodium} parasites. Although their study focused on just \textit{P. falciparum}, the model built using a combination of shape, color and texture (Haar) features obtained an accuracy of 92\%. Moreover, the study packages the algorithm in an autoscope, a low-cost automated microscope.  The autoscope can scan a full thick smear and process the captured image in under 20 minutes.

With the popularity of deep learning models recently, many malaria-related research used convolutional neural networks to identify \textit{Plasmodium} parasites. Quinn, et. al \cite{Quinn}, working on thick blood smears, designed a four layer convolutional neural network for general microscopy diagnosis. Their architecture involved convolutional layers with seven 3x3 and 12 2x2 filters. Aside from the algorithm, their research also came up with a hardware component to capture image data directly from the microscope using smartphones for analysis.

Several convolutional neural network architectures were built and are being built. With this, Dong, et. al \cite{Dong} evaluated three pre-trained models using transfer learning on the identification of malaria parasites. These models include LeNet, AlexNet and GoogLeNet. Aside from CNN models, they also included an SVM model with color and shape features in their evaluation. Their results show that all CNN models outperform the SVM model by a significant margin with the advantage of minimal input from experts as features are not manually engineered. Moreover, their study also showed that out of the three CNN models, GoogLeNet is the best fit for the task as it obtained the highest accuracy.

Liang, et. al \cite{Liang}, on the other hand, analyzed the performances of custom-trained convolutional neural networks and models built using transfer learning. They compared AlexNet and their own model on identification of malaria parasites. The study concludes that custom models is superior than transfer learning models in that specific classification task but at the expense of longer training time.
\section{Shape Descriptors}  \label{sec:shapedescriptors}
Aside from color analysis, another recurring process in the studies reviewed (Chapter \ref{sec:StateoftheArt}) is shape analysis. Shape analysis is the automatic analysis and description of geometric shapes in digital images. Each region of interest in a digital image is characterized by a vector of values representing some measurements. Some of the measurements used in shape analysis are the following:
\begin{itemize}
	\item Area
	\item Convex Area
	\item Eccentricity
	\item Equivalent Diameter
	\item Euler Number
	\item Extent
	\item Major Axis Length
	\item Minor Axis Length
	\item Orientation
	\item Perimeter
	\item Solidity
\end{itemize}

\subsection*{Area} \label{sec:Area}
In shape analysis of a digital image, the area is given by the number of pixels contained in the region. For a binary image, with 1 representing the foreground, the area is equal to the number of pixels whose value is 1 \cite{Regionprops}. In equation form,
\begin{equation}
Area(I) = \sum_{i=1}^{m}{\sum_{j=1}^{n}{ I(i,j)}}
\end{equation}
where \textit{m} and \textit{n} are the width and height of the region, in pixels, and\\
\textit{I(i,j)} is the pixel value at row \textit{i}, column \textit{j} of the region.
\subsection*{Convex Area}
Convex Area is computed in the same manner as Area except a convex image is used instead of the original. A convex image specifies convex hulls containing parts of the region of interest, with all the holes filled in \cite{Regionprops}.
\subsection*{Eccentricity}
The eccentricity of the region of interest in a digital image is the eccentricity of the ellipse with the same second moment as the region \cite{Regionprops}. The eccentricity of the ellipse is given by
\begin{equation}
Eccentricity = \frac{distance\, between\, foci}{major\, axis\, length}
\end{equation}
\subsection*{Equivalent Diameter}
Equivalent diameter is the diameter of the circle with the same area as the region \cite{Regionprops}. It is computed as follows:
\begin{equation}
Equivalent\, Diameter = \sqrt{\dfrac{4Area}{\pi}}
\end{equation}
\subsection*{Euler Number}
Euler number is the number of connected components minus the number of holes in the components \cite{Regionprops}.
\subsection*{Extent}
Extent is the ratio of the computed area to the area of the bounding box of the region \cite{Regionprops}.
\begin{equation}
Extent = \dfrac{Area}{Bounding\, box\, area}
\end{equation}
\subsection*{Major Axis Length}
Major axis length is the length of the major axis of the ellipse with the same second moment as the region \cite{Regionprops}.
\begin{center}
	\includegraphics[width=2in]{major-axis.png}
\end{center}
\subsection*{Minor Axis Length}
Minor axis length is the length of the minor axis of the ellipse with the same second moment as the region \cite{Regionprops}.
\begin{center}
	\includegraphics[width=2in]{minor-axis.png}
\end{center}
\subsection*{Orientation}
The orientation of the region of interest is the angle the major axis of the ellipse with the same second moment as the region makes with respect to the x-axis as shown below. \cite{Regionprops}.
\begin{center}
	\includegraphics{orientation.jpg}
\end{center}
\subsection*{Perimeter}
Perimeter is the distance around the boundary of the region of interest. The figure below illustrates the perimeter of a region. \cite{Regionprops}.
\begin{center}
	\includegraphics[width=2in]{perimeter.jpg}
\end{center}
\subsection*{Solidity}
Solidity is computed as follows \cite{Regionprops}:
\begin{equation}
Solidity = \frac{Area}{Convex\, Area}
\end{equation}

%[explain why they are useful - sir pros]

\section{Deep Convolutional Neural Networks} \label{sec:cnn}
%
% http://cs231n.github.io/convolutional-networks/
%
% C. Szegedy, V. Vanhoucke, S. Ioffe, J. Shlens, and Z. Wojna.
% Rethinking the inception architecture for computer vision.
% arXiv preprint arXiv:1512.00567, 2015.

% C. Szegedy, W. Liu, Y. Jia, P. Sermanet, S. Reed,
% D. Anguelov, D. Erhan, V. Vanhoucke, and A. Rabinovich.
% Going deeper with convolutions. In Proceedings of the IEEE
% Conference on Computer Vision and Pattern Recognition,
% pages 1–9, 2015
%
Convolutional Neural Networks or CNNs (shown in Figure \ref{fig:cnn}) are very similar to ordinary artificial neural networks having trainable weights on sets of neurons given some inputs and expected outputs. One key difference is that in CNNs, the input are three-dimensional data (height, width and depth) such as images. This additional dimension (depth) would require a restructuring of neurons in ANNs and a huge additional computational complexity, giving rise to another key difference of CNNs and ANNs: the weight of the neurons are shared. Layers in a CNN also differ in function and are often classified into one of the following \cite{CS231}:
\begin{itemize}
	\item Input Layer
	\item Convolutional Layer
	\item Non-linearity Layer
	\item Pooling Layer
	\item Fully Connected Layer
\end{itemize}
\begin{figure}[h]
	\begin{center}
		\includegraphics[width=5in]{cnn.jpeg}
	\end{center}
	\caption{Convolutional Neural Network}
	\caption*{\textit{Source:} CS231n Convolutional Neural Networks for Visual Recognition \cite{CS231}}
	\label{fig:cnn}
\end{figure}
The input layer accepts the volumetric data (image) \cite{CS231}. 

Convolutional layers, in turn, will compute activations/convolutions of local regions in the input by performing a dot product of the weights of the neurons (filter) and the local region \cite{CS231}.  A non-linearity layer applies an element-wise non-linear function such as max(0,x) \cite{CS231}. 

Pooling downsamples a volume and shrinks it spatially. Pooling has been shown to prevent overfitting of the model to the training data. Usual pooling operation shrinks the width and height by two. For example, the pooling layer gets an input volume of 32x32x12. After pooling, the volume gets downsampled to 16x16x12 - a reduction by 2 on both width and height \cite{CS231}. Lastly, a fully connected layer performs like an ordinary neural network, computing for the class scores for classification \cite{CS231}.

Different architectures over the few years of active research on CNNs were proposed. These architectures mainly differ on the number of layers for each type used, and the parameter values used for each \cite{CS231}. 

LeNet is regarded as the first successful application of convolutional neural network. This specific architecture was developed by Yann LeCun back in 1990s to read zip codes and digits \cite{CS231}.

Although LeNet came first, it was AlexNet that popularized the use of convolutional neural networks and laid out a baseline for comparison on the performance of different architectures. Developed by Alex Krizhevsky, et. al, AlexNet performed well on the ImageNet Large Scale Visual Recognition Challenge (ILSVRC) 2012 with a top-5 error of 16\% \cite{Szegedy}.

With the success of AlexNet, several research on new CNN models were made, most of them focusing on building deeper and wider networks. These incude VGGNet and GoogLeNet which performed very well on ILSVRC 2014 \cite{Szegedy}.

GoogLeNet employs the Inception architecture that aims to lessen the computation complexities of its precursor architectures. For example, AlexNet uses 60 million parameters while VGGNet requires 3x more. This large set of parameters directly translates to large computational cost, slower training and classification \cite{Szegedy, Szegedy2}. 

Inception tries to minimize the set of parameters by factorizing convolutions with large filter size into a set of multiple convolutions with small filter sizes. For example, a convolution of 5x5 convolution is replaced by two 3x3 convolutions in Inception. Developers of Inception have shown that this replacement is indeed less computationally complex than the original convolution. With only around 13 million parameters, Inception (version 3)  outperformed VGGNet, with only 3.58\% top-1 error and 17.2\% top-5 error. \cite{Szegedy2}
