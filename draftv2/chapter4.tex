\chapter{Methodology and Results}
%
%This part of the thesis is much more free-form. It may have one or several
%sections and subsections. But it all has only one purpose: to convince the
%examiners that you answered the question or solved the problem that you
%set for yourself in Chapter~\ref{sec:ResearchQ}. So show what you did that is relevant to answering the question or solving the problem: if there were blind alleys and dead ends, do not include these, unless specifically relevant to the
%demonstration that you answered the thesis question.

In this chapter, we provide a detailed presentation of the steps taken, alongside results - from data processing and image segmentation to the application of the proposed classification scheme for malaria species identification.
%data gathering
\section{Dataset}
The dataset used is composed of 363 images. The smears used for the images were obtained from one of the field works in Palawan, Philippines of the College of Public Health, University of the Philippines. Images were taken using a digital single-lens reflex camera attached to a microscope, giving a resolution of 2592$\times$1944 pixels at magnification using $100\times$ oil immersion objective lens.

To fully capture the entire erythrocytic schizogony (see Chapter \ref{sec:lifecycle}) of malaria parasites, key lifecycle stages (ring-form and gametocyte) were made sure to be represented in the dataset.

Images of \textit{P. vivax} were part of the dataset to effectively discriminate \textit{P. falciparum} from this species. From Chapter \ref{sec:diagnosis}, a person with \textit{P. falciparum} requires a different treatment compared to one with other species. Table \ref{table:imagespecies} shows the breakdown of images per species.

\begin{table}[h]
\centering
\begin{tabular}{|c|c|}
\hline \textbf{Species} & \textbf{Number of Images}\\
\hline \textit{P. falciparum} & 221\\
\hline \textit{P. vivax} & 142\\
\hline
\end{tabular}
\caption{Number of images per species}
\label{table:imagespecies}
\end{table}
%preprocessing
\section{Data Preprocessing}
As shown in literature reviewed in Chapter \ref{sec:StateoftheArt}, specifically that of Makkapati and Rao \cite{Makkapati-Rao}, the HSV color space is better than the RGB color space for the segmentation task to follow. In this study, the saturation channel was used since it gives the best contrast between parasite and non-parasite regions in the images.

A series of morphological transforms were done for data preprocessing to enhance the digital images before segmentation. 

Each image underwent image opening and closing. This step ensures that salt-and-grain noise that would otherwise register as candidate regions were removed. A circular structuring element was used for these operations on the assumption that the objects in the images are more or less circular in shape. Figure \ref{fig:imageopenclose} shows one image before and after image opening and closing.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=5in]{iocbefore.png}\vspace{5pt}
		\includegraphics[width=5in]{iocafter.png}
	\end{center}
	\caption{Image before and after image opening and closing}
	\label{fig:imageopenclose}
\end{figure}

After which, the mean and standard deviation of each image is computed and subtracted from the image. Upon observation of the saturation values, pixels belonging to the parasite regions have saturation values above the mean saturation value. Figure \ref{fig:imagemeanstd} shows one image before and after subtraction of mean and standard deviation.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=5in]{iocafter.png}\vspace{5pt}
		\includegraphics[width=5in]{imstd.png}
	\end{center}
	\caption{Image before and after subtraction of mean and standard deviation}
	\label{fig:imagemeanstd}
\end{figure}

Lastly, binary thresholding was done. Any saturation value below 20 was considered 0 (the background) and values more than or equal to 20 were considered 1 (the foreground). Figure \ref{fig:imagethresholding} shows one image before and after thresholding. Figure \ref{fig:preprocessing} shows an image before and after all preprocessing steps.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=5in]{imstd.png}\vspace{5pt}
		\includegraphics[width=5in]{ithresh.png}
	\end{center}
	\caption{Image before and after binary thresholding}
	\label{fig:imagethresholding}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=5in]{1corig.JPG}\vspace{5pt}
		\includegraphics[width=5in]{1c.JPG}
	\end{center}
	\caption{An image before and after preprocessing}
	\label{fig:preprocessing}
\end{figure}

As seen from Figure \ref{fig:preprocessing}, not only the parasite regions were captured in the foreground but also some regions like blood artefacts and excess stains. These artefacts and excess stains belong to the negative class in this study.
%segmentation
\section{Image Segmentation}
After the preprocessing step, segmentation was done through connected components analysis. For each region with area greater than 500 pixels, a square bounding box was used for segmentation. Figure \ref{fig:segment} shows some segmented regions. These regions vary in size and required rescaling to a standard size of 299$\times$299 pixels for some of the succeeding steps.
\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=1in]{s1.jpg}
		\includegraphics[width=1in]{s2.jpg}
		\includegraphics[width=1in]{s3.jpg}
		\includegraphics[width=1in]{s4.jpg}
		\includegraphics[width=1in]{s5.jpg}
		\includegraphics[width=1in]{s6.jpg}
	\end{center}
	\caption{Some segmented regions}
	\label{fig:segment}
\end{figure}
\section{Data Augmentation}
As is to be expected, segmentation would yield far numerous non-malaria regions than malaria. This poses a problem especially for classifiers relying on minimization of some loss function. This imbalance could result to converging to a local minimum, however, augmentation helps prevent this by artificially creating additional instances through some transformations like rotation, translation, and scaling. 

To augment the dataset, rotation by 15 degree increments were done on all malaria regions. Figure \ref{fig:augment} shows a malaria region rotated 23 times to produce additional artificial malaria instances.

The number of regions to be classified after performing augmentation are shown in Table \ref{table:augmentation}.
\begin{table}
	\centering
	\begin{tabular}{|c|c|}
		\hline \textbf{Class} & \textbf{Number of Images}\\
		\hline P. \textit{falciparum} trophozoite & 4264\\
		\hline P. \textit{falciparum} gametocyte & 3360\\
		\hline P. \textit{vivax} trophozoite & 4680\\
		\hline Negative & 4501\\
		\hline
	\end{tabular}
	\caption{Number of image regions per class after augmentation}
	\label{table:augmentation}
\end{table}
\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.85in]{0.jpg}
		\includegraphics[width=0.85in]{15.jpg}
		\includegraphics[width=0.85in]{30.jpg}
		\includegraphics[width=0.85in]{45.jpg}
		\includegraphics[width=0.85in]{60.jpg}
		\includegraphics[width=0.85in]{75.jpg}\vspace{4pt}
		\includegraphics[width=0.85in]{90.jpg}
		\includegraphics[width=0.85in]{105.jpg}
		\includegraphics[width=0.85in]{120.jpg}
		\includegraphics[width=0.85in]{135.jpg}
		\includegraphics[width=0.85in]{150.jpg}
		\includegraphics[width=0.85in]{165.jpg}\vspace{4pt}
		\includegraphics[width=0.85in]{180.jpg}
		\includegraphics[width=0.85in]{195.jpg}
		\includegraphics[width=0.85in]{210.jpg}
		\includegraphics[width=0.85in]{225.jpg}
		\includegraphics[width=0.85in]{240.jpg}
		\includegraphics[width=0.85in]{255.jpg}\vspace{4pt}
		\includegraphics[width=0.85in]{270.jpg}
		\includegraphics[width=0.85in]{285.jpg}
		\includegraphics[width=0.85in]{300.jpg}
		\includegraphics[width=0.85in]{315.jpg}
		\includegraphics[width=0.85in]{330.jpg}
		\includegraphics[width=0.85in]{345.jpg}
	\end{center}
	\caption{Dataset augmentation by rotation of segmented image}
	\label{fig:augment}
\end{figure}
%feature extraction
\section{Feature Extraction} \label{sec:shapefeatures}
Shape features were then extracted from each segmented region. The following measurements were taken as features characterizing the shape of each segmented region:
\begin{itemize}
\item Area
\item ConvexArea
\item Eccentricity
\item Equivalent Diameter
\item Euler Number
\item Extent
\item Filled Area
\item Major Axis Length
\item Minor Axis Length
\item Orientation
\item Perimeter
\item Solidity
\end{itemize}
The axes lengths would be useful in identifying \textit{P. falciparum} gametocytes as these features would characterize the sickle shape of the infected cells. The areas, perimeter and solidity features could be used to distinguish white blood cells and other artefacts.
\section{Identification of \textit{P. falciparum} gametocytes}
%The proposed model for malaria parasite identification is two-tiered. The first level tries to classify the region based on size. If the region satisfies the threshold size, it is considered for \textit{P. falciparum} gametocyte identification. Otherwise, the region is considered for ring form identification.

%Figure \ref{fig:classificationtree} shows the classification scheme used. This classification scheme is used on the assumption that input images are of the same magnification, such that the apparent difference in size of parasites in gametocyte stage and trophozoite stage could be used to divide the classification problem into two smaller ones: P. \textit{falciparum} gametocyte vs non-P. \textit{falciparum} gametocyte and P. \textit{falciparum} trophozoite vs. P. \textit{vivax} trophozoite vs non-trophozoite.
The first classification task is to identify \textit{P. falciparum} gametocytes. This is done to serve as baseline for comparison with some literature reviewed focusing on the identification of this species at this specific life stage. This task also serves as an initial test that would show if the features extracted, on both shape analysis and CNN, are discriminant even on a subset of the whole classification task.

Shape features extracted from the previous section (\ref{sec:shapefeatures}) were tested on a classification model using an artificial neural network (ANN) with three fully connected layers. This ANN is configured with 2048 hidden nodes and the sigmoid function as activation function. This ANN is trained for 100000 iterations with a learning rate of 0.7.

\begin{table}[!h]
	\centering
	\begin{tabular}{|p{1.5in}|c|}
		\hline Accuracy & 96.26\% \\
		\hline Specificity & 99\% \\
		\hline Sensitivity & 78\% \\
		\hline
	\end{tabular}
	\caption{Performance evaluation on Shape Feature - ANN trained to identify \textit{P. falciparum} gametocyte}
	\label{table:shapefalci}
\end{table}

Table \ref{table:shapefalci} shows the performance of the artificial neural network trained to identify \textit{P. falciparum} gametocyte. It is clear from this table that the ANN with shape features yielded very high accuracy (96.26\%), significantly higher than the accuracy obtained by Pinkaew, et. al (85.71\%) \cite{Pinkaew} in identifying \textit{P. falciparum}, but upon close inspection on the sensitivity value (78\%), it seems to be biased towards the negative class. 

Aside from shape features, features generated by a convolutional neural network were investigated for malaria detection and species classification.

Utilizing transfer learning on Inception v3 convolutional neural network, a separate model identifying \textit{P. falciparum} gametocytes was built. The model used the available pre-trained Inception v3 CNN weights but retrained the  last three fully-connected layers (multi-layer perceptron) equivalent to the ANN used for the shape features previously. Inception v3 is configured to generate 2048 features as input to its multi-layer perceptron classifier.

\begin{table}[!h]
	\centering
	\begin{tabular}{|p{1.5in}|c|}
		\hline Accuracy & 97.99\% \\
		\hline Specificity & 96.82\% \\
		\hline Sensitivity & 98.62\% \\
		\hline
	\end{tabular}
	\caption{Performance evaluation on CNN model trained to identify \textit{P. falciparum} gametocyte}
	\label{table:cnnfalcieval}
\end{table}

Table \ref{table:cnnfalcieval} shows the accuracy, specificity, and sensitivity of the CNN trained to identify \textit{P. falciparum} gametocyte. The same with the ANN model, the accuracy obtained is significantly higher than that of Pinkaew, et. al (85.71\%) \cite{Pinkaew}.

Comparing the performance of the CNN with the ANN using shape features, it is very clear from the specificity and sensitivity values of CNN that it outperforms its ANN counterpart. Another point of comparison worth noting is the time spent to extract features used and train the two separate models. Using the same hardware discussed in Appendix \ref{appendix:hardware}, the ANN trained for 5 hours while the CNN was built in under 30 minutes.

% compare with literature
\section{Detection and Identification of Malaria Species}

Another model using convolutional neural network was built, this time to classify malaria species. Retraining of an Inception v3 convolutional neural network was performed again for this model.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=5in]{cnntraintroph.png}
	\end{center}
	\caption{Training accuracy per epoch of the CNN trained to classify \textit{Plasmodium} species}
	\label{fig:cnnfalci2}
\end{figure}

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|}
	\hline Train accuracy &  94\%\\
     	\hline Cross entropy & 0.22\\
     	\hline Validation accuracy & 87.6\%\\
     	\hline 
	\end{tabular}
	\caption{Training and validation results of the CNN model trained to detect \textit{Plasmodium} parasites}
	\label{table:cnntrain}
\end{table}

Figure \ref{fig:cnnfalci2} shows the training accuracy per epoch of the CNN trained to classify images as \textit{P. falciparum} trophozoite, \textit{P. falciparum} gametocyte, \textit{P. vivax} trophozoite, or negative. Table \ref{table:cnntrain} shows the training results of the model. Positive detection was  simplified to classification to any from one of the three classes: \textit{P. vivax}, \textit{P. falciparum} gametocyte and \textit{P. falciparum} ring-form. The model obtained a training accuracy of 94\% in detecting parasites on images and 87.6\% validation accuracy. 

Running on the test set, the model obtained an accuracy of 92.3678\% in parasite detection (with 95.2008\% sensitivity and 84.7307\% specificity) and 87.95\% in \textit{Plasmodium} species identification. Table \ref{table:cnnfalcieval} shows the confusion matrix of the CNN trained to identify \textit{Plasmodium} species on the test set.

\begin{table}[!h]
	\centering
	\begin{tabular}{|p{1in}|l|p{1in}|p{1in}|l|}
	\hline  & VIVAX & FALCIPARUM GAMETOCYTE  & FALCIPARUM RING-FORM & NEGATIVE\\
	\hline VIVAX & \textbf{1654} & 46 & 92 & 80\\
	\hline FALCIPARUM GAMETOCYTE & 33 & \textbf{1214} & 1 & 24\\
	\hline FALCIPARUM RING-FORM & 111 & 11 & \textbf{1460} & 129\\
	\hline NEGATIVE & 88 & 26 & 161 & \textbf{1526}\\
	\hline
	\end{tabular}
	\caption{Performance evaluation on CNN model trained to identify \textit{Plasmodium} species}
	\label{table:cnnfalcieval}
\end{table}

The CNN model has an accuracy of 87.67\% in \textit{P. falciparum} identification and 88.35\% in \textit{P. vivax} identification. In comparison with the SVM model by Pinkaew, et. al on  \textit{P. falciparum} and \textit{P. vivax} identification with 85.72\% and 78.72\%, respectively, the CNN model built in this study outperforms the SVM model on both tasks.

\begin{figure}[ht]
	\includegraphics[width=3in]{annotate.jpg}
	\label{fig:annotated}
	\caption{An annotated image after classification}
\end{figure}