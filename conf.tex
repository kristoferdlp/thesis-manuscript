\documentclass[conference]{IEEEtran}

\usepackage[pdftex]{graphicx}
\usepackage[cmex10]{amsmath}
\hyphenation{op-tical net-works semi-conduc-tor}
\usepackage[backend=bibtex,sorting=none,maxnames=6]{biblatex}

\bibliography{biblio.bib}

\begin{document}
\bstctlcite{biblio}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Malaria Parasite Detection and Species Identification on Thin Blood Smears using Convolutional Neural Networks}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{\IEEEauthorblockN{Kristofer E. delas Pe\~nas, Pilarita T. Rivera and Prospero C. Naval, Jr.}
\IEEEauthorblockA{Computer Vision and Machine Intelligence Group\\
Department of Computer Science\\
University of the Philippines, Diliman\\
Quezon City, Philippines\\
Email: pcnaval@dcs.upd.edu.ph}}
% make the title area
\maketitle



\begin{abstract}
%\boldmath
To aid the efforts on the elimination of malaria, effective and fast diagnosis of cases must be done. The gold standard for malaria diagnosis still remain to be microscopy. This process becomes problematic and time-consuming when cases come from far-flung areas as experts may not be present in those areas to make a diagnosis. Automation of the diagnosis process with the use of an intelligent system that would recognize malaria parasites could solve this problem. This research proposes such an intelligent system, detecting malaria parasites and the specific species on images of thin blood smears. The model in this research got an accuracy of 92.4\% and sensitivity of 95.2\% in detecting malaria parasites, and an accuracy of 87.9\% in identifying the species Plasmodium \textit{falciparum} and Plasmodium \textit{vivax}.

\begin{IEEEkeywords}
Malaria, Intelligent Systems, Convolutional Neural Network
\end{IEEEkeywords}
\end{abstract}
\IEEEpeerreviewmaketitle

\section{Introduction}
% malaria statistics gjjkgkjnlhafsdlbl
In 2012, the World Health Organization (WHO) estimated 207,000,000 cases of malaria globally, 627,000 of which resulted to death among African children \cite{WHO2}. In the Philippines, malaria is considered to be the 9th leading cause of morbidity, with 58 out of the 81 provinces being malaria-endemic \cite{DOH}.

One critical point in malaria mitigation is the remote location of majority of the cases and the unavailability of trained individuals to analyze blood samples under the microscope to diagnose. This is where automated systems for diagnosis come in - instead of manually going over a blood sample, checking for the presence of malaria parasites, photographs of the sample viewed from the microscope is analyzed by an intelligent system without or with minimal intervention by an individual trained in microscopy. %citation pls
With such systems, the remote location of malaria cases becomes less of a problem if such systems become publicly available as trained microsopists and doctors need not be physically present to come up with a diagnosis.

This study aims to build one such system - an intelligent system to recognize malaria parasites and the specific species in blood samples.

\section{Malaria Parasites} \label{sec:species}
\textit{Plasmodium vivax} is the cause of most malaria cases, accounting for around 80\% of the total infections in Asia and America. This species causes benign tertian malaria with frequent lapses \cite{Paniker}. 

The parasitemia of \textit{P. vivax} is usually not heavy. In blood samples, infected erythrocytes would contain only one trophozoite, and with less than 5\% of the cells being infected. In its trophozoite stage, the structure of the parasite is well-defined, having a prominent central vacuole. The cytoplasm is distinguishably blue, while the nucleus red in stained films. An infected erythrocyte would be slightly enlarged and exhibits red granulations called \textit{Schuffner's dots} \cite{Paniker}.

\textit{Plasmodium falciparum} is most distinguishable for its sickle-shaped gametocyte. This species is the most pathogenic of all species, posing high rate of complications and fatality if left untreated \cite{Paniker}. The trophozoites of this species has very delicate and appear as tiny ring-forms usually attached near the walls of the erythrocytes. Many of the ring-forms have binucleate rings (double chromatin dots) \cite{Paniker}.

The diagnosis of malaria is done through demonstration of malaria parasites in the blood by preparing blood smears. A thin smear is used in determining the infecting species. This type of smear is prepared from capillary blood drawn from a fingertip, applied to a slide, and stained by one of the Romanowsky stains such as \textit{Giemsa} to highlight the parasite \cite{Paniker}. 

Figure \ref{thinsmear} shows an example of a thin smear.

\begin{figure}[h]
	\begin{center}
	\includegraphics[width=3in]{thinsmear.jpg}
	\caption{A thin smear}
	\label{thinsmear}
	\end{center}
\end{figure}

Treating malaria requires administration of anti-malarial drugs. The infecting species of malaria parasite play a significant role on the course of treatment. P. \textit{falciparum} and P. \textit{vivax} are treated differently. Hence, the need for species identification.
\section{State of the Art Intelligent Systems for Malaria} \label{sec:StateoftheArt}

Intelligent systems for malaria have been developed with aims to automate the diagnosis and classification of blood slides, and to counter-check, and even eliminate human errors. To do this, images of the slides viewed from a microscope needs to be taken and used as input for these systems.

Various techniques in digital image processing and computer vision were employed in a lot of scientific researches focused on malaria detection and classification. One main step in detecting the presence of malaria parasites in an image of a blood sample is to segment possible regions of interest(foreground) from the background. The background for the malaria images is a large region of stained blood plasma, and depending on the quality of staining performed and the image captured, the color of the background varies from a bright pink to a dull gray tone. Figure \ref{malariatones} shows very different background colors between two image samples.
\begin{figure}
\centering
\includegraphics[width=3in]{malariatone1.jpg}
\includegraphics[width=3in]{malariatone2.jpg}
\caption{Difference in background color of two images of blood smears}
\label{malariatones}
\end{figure}
%review papers here
%segmentation
One research focused on segmentation is done by Makkapati and Rao \cite{Makkapati-Rao}. The research addressed the problems due to variability and presence of artifacts in microscope images of blood samples. The authors used a segmentation scheme based on the HSV color space to separate the red blood cells (RBC) and parasites. This scheme focuses on detecting the parasites' chromatin within the RBCs once the segmentation was done. The HSV color space was used since the RGB color space is not intuitive for processing unlike HSV, which offers a representation that could be used to easily identify among color families even with varying image qualities. Given 55 test images, the specificity and sensitivity of this approach are 83\% and 98\%.

A similar research was done by Anggraini, et. al \cite{Anggraini}. The proposed algorithm does not consider for segmentation the hue of the individual pixels of the image. Instead, a global threshold was obtained by varying the contrast among pixels, and was used to classify each pixel as belonging to either the foreground or the background. To do this, pre-processing of the images to obtain `uniform' images was done by converting all input images to grayscale. After which, the images were filtered to normalize the pixel intensities around a median value, before proceeding with obtaining the histogram of the individual images, and expanding each until the two intensity classes: foregound and background, becomes distinct given a threshold intensity.

%morphological approaches
Some research employ more morphological techniques than the two previous ones. For one, the work done by Das, et. al \cite{Das} uses thresholding, marker-controlled watershed algorithm, and Haralick textural feature extraction.
Another work, by Kareem, et. al \cite{Kareem}, also uses morphological image transformations to infer malaria parasites from Giemsa-stained blood films. First, a grayscale image is dilated and eroded to highlight the parasites and platelets (foreground). The cells and parasites are then identified based on a combination of annular ring ratio method, size, and intensity variation.

In conjunction with the techniques explored in the previously mentioned researches, machine learning techniques serve as the core for the decision systems for malaria. Some, like Das et. al \cite{Das}, employ generative methods such as multivariate regression models. Anggraini, et. al \cite{Anggraini}, used Bayes decision theory to classify images after segmentation.

Other researchers use discriminative models to classify malaria images. One such research is done by Barros, et. al \cite{Barros}. This particular research used artificial neural networks, aside for Bayesian networks, to construct a model to diagnose asymptomatic malaria. 

Pinkaew et. al explored the use of support vector machines in malaria classification \cite{Pinkaew}. Their work used statistical measurements such as mean intensity, standard deviation, kurtosis, skewness and entropy to characterize regions of interest. Training on 40 P. \textit{falciparum} and 25 P. \textit{vivax} images, the study's SVM model with radial basis kernel gained an accuracy of 85.71\% in identifying P. \textit{falciparum} and 78.72\% in identifying P. \textit{vivax}.
\section{Convolutional Neural Network}
Convolutional Neural Networks or CNNs (shown in Figure \ref{fig:cnn}) are very similar to ordinary artificial neural networks having trainable weights on sets of neurons given some inputs and expected outputs. One key difference is that in CNNs, the input are three-dimensional data (height, width and depth) such as images. This additional dimension (depth) would require a restructuring of neurons in ANNs and a huge additional computational complexity, giving rise to another key difference of CNNs and ANNs: the weight of the neurons are shared \cite{CS231}.
\begin{figure}[h]
	\begin{center}
		\includegraphics[width=3in]{cnn.jpeg}
	\caption{Convolutional Neural Network\newline\tiny{\textit{Source:} CS231n Convolutional Neural Networks for Visual Recognition.\newline URL: \textit{http://cs231n.github.io/convolutional-networks/}}}
	\label{fig:cnn}
	\end{center}
\end{figure}

Different architectures over the few years of active research on CNNs were proposed. These architectures mainly differ on the number of layers for each type used, and the parameter values used for each \cite{CS231}. GoogLeNet employs the Inception architecture that aims to lessen the computation complexities of its precursor architectures. For example, AlexNet uses 60 million parameters while VGGNet requires 3x more. This large set of parameters directly translates to large computational cost, slower training and classification \cite{Szegedy, Szegedy2}. 

Inception tries to minimize the set of parameters by factorizing convolutions with large filter size into a set of multiple convolutions with small filter sizes. For example, a convolution of 5x5 convolution is replaced by two 3x3 convolutions in Inception. Developers of Inception have shown that this replacement is indeed less computationally complex than the original convolution. With only around 13 million parameters, Inception (version 3)  outperformed VGGNet, with only 3.58\% top-1 error and 17.2\% top-5 error. \cite{Szegedy2}
\section{Methodology and Results}
\subsection{Dataset}
\begin{table}
	\centering
	\begin{tabular}{|c|c|}
		\hline \textbf{Number of Images in Dataset} & 363\\
		\hline \textbf{Image Resolution} & 2592 x 1944 pixels\\
		\hline \textbf{P. \textit{falciparum} : P. \textit{vivax} ratio} & 221:142\\
		\hline
	\end{tabular}
	\caption{The dataset used in the study}
	\label{table:dataset}
\end{table}
The dataset used is composed of 363 images. The smears used for the images were obtained from one of the field works in Palawan, Philippines of the College of Public Health, University of the Philippines. Images were taken using a digital single-lens reflex camera attached to a microscope, giving a resolution of 2592 x 1944 pixels at magnification using 100x oil immersion objective lens.

To fully capture the entire erythrocytic schizogony of malaria parasites, key lifecycle stages (trophozoite and gametocyte) were made sure to be represented in the dataset.

Images of P. \textit{vivax} (142 images) were part of the dataset to effectively discriminate P. \textit{falciparum} (221 images) from this species.

Table \ref{table:dataset} summarizes the information on the dataset used in this study.
%preprocessing
\subsection{Data Preprocessing}
As shown in literature reviewed, specifically that of Makkapati and Rao \cite{Makkapati-Rao}, the HSV color space is better than the RGB color space for the segmentation task to follow. In this study, the saturation channel was used since it gives the best contrast between parasite and non-parasite regions in the images.

A series of morphological transforms were done for data preprocessing to enhance the digital images before segmentation. 

Each image underwent image opening and closing using a circular structuring element. This step ensures that salt-and-grain noise that would otherwise register as candidate regions were removed.Figure \ref{fig:imageopenclose} shows one image before and after image opening and closing.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=3in]{iocbefore.png}\vspace{5pt}
		\includegraphics[width=3in]{iocafter.png}
	\end{center}
	\caption{Image before and after image opening and closing}
	\label{fig:imageopenclose}
\end{figure}

The mean and standard deviation of each image is then computed and subtracted from the image. Figure \ref{fig:imagemeanstd} shows one image before and after subtraction of mean and standard deviation.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=3in]{iocafter.png}\vspace{5pt}
		\includegraphics[width=3in]{imstd.png}
	\end{center}
	\caption{Image before and after subtraction of mean and standard deviation}
	\label{fig:imagemeanstd}
\end{figure}

Lastly, binary thresholding was done. Figure \ref{fig:imagethresholding} shows one image before and after thresholding.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=3in]{imstd.png}\vspace{5pt}
		\includegraphics[width=3in]{ithresh.png}
	\end{center}
	\caption{Image before and after binary thresholding}
	\label{fig:imagethresholding}
\end{figure}

Figure \ref{fig:preprocessing} shows an image before and after all preprocessing steps.
\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=3in]{1corig.JPG}\vspace{5pt}
		\includegraphics[width=3in]{1c.JPG}
	\end{center}
	\caption{An image before and after preprocessing}
	\label{fig:preprocessing}
\end{figure}
%segmentation
\subsection{Image Segmentation}
After the preprocessing step, segmentation was done through connected components analysis. For each region with area greater than 500 pixels, a square bounding box was used for segmentation. Figure \ref{fig:segment} shows some segmented regions. These regions vary in size and required rescaling to a standard size of 299x299 pixels for some of the succeeding steps.
\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=1in]{s1.jpg}
		\includegraphics[width=1in]{s2.jpg}
		\includegraphics[width=1in]{s3.jpg}\vspace{4pt}
		\includegraphics[width=1in]{s4.jpg}
		\includegraphics[width=1in]{s5.jpg}
		\includegraphics[width=1in]{s6.jpg}
	\end{center}
	\caption{Some segmented regions}
	\label{fig:segment}
\end{figure}
\subsection{Data Augmentation}
As is to be expected, segmentation would yield far numerous non-malaria regions than malaria. This poses a problem especially for classifiers relying on minimization of some loss function. This imbalance could result to converging to a local minimum, however, augmentation helps prevent this by artificially creating additional instances through transformations like rotation, translation, and scaling. To augment the dataset, rotation by 15 degree increments were done on all malaria regions. Figure \ref{fig:augment} shows a malaria region rotated 23 times to produce additional malaria instances.
\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.5in]{0.jpg}
		\includegraphics[width=0.5in]{15.jpg}
		\includegraphics[width=0.5in]{30.jpg}
		\includegraphics[width=0.5in]{45.jpg}
		\includegraphics[width=0.5in]{60.jpg}
		\includegraphics[width=0.5in]{75.jpg}\vspace{4pt}
		\includegraphics[width=0.5in]{90.jpg}
		\includegraphics[width=0.5in]{105.jpg}
		\includegraphics[width=0.5in]{120.jpg}
		\includegraphics[width=0.5in]{135.jpg}
		\includegraphics[width=0.5in]{150.jpg}
		\includegraphics[width=0.5in]{165.jpg}\vspace{4pt}
		\includegraphics[width=0.5in]{180.jpg}
		\includegraphics[width=0.5in]{195.jpg}
		\includegraphics[width=0.5in]{210.jpg}
		\includegraphics[width=0.5in]{225.jpg}
		\includegraphics[width=0.5in]{240.jpg}
		\includegraphics[width=0.5in]{255.jpg}\vspace{4pt}
		\includegraphics[width=0.5in]{270.jpg}
		\includegraphics[width=0.5in]{285.jpg}
		\includegraphics[width=0.5in]{300.jpg}
		\includegraphics[width=0.5in]{315.jpg}
		\includegraphics[width=0.5in]{330.jpg}
		\includegraphics[width=0.5in]{345.jpg}
	\end{center}
	\caption{Dataset augmentation by rotation of segmented image by 15 degree increments}
	\label{fig:augment}
\end{figure}
\subsection{Classification}
A convolutional neural network was used for malaria species classification. Utilizing transfer learning on Inception v3 convolutional neural network, a model was built to classify the Plasmodium species. After creating bottleneck values from the convolutional layers of the network, the fully-connected layers was retrained to classify the species.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=3.5in]{cnntraintroph.png}
	\end{center}
	\caption{Training accuracy per epoch of the CNN trained to classify trophozoite species}
	\label{fig:cnnfalci2}
\end{figure}


\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|}
		\hline Train accuracy &  94\%\\
     	\hline Cross entropy & 0.22\\
     	\hline Validation accuracy & 87.6\%\\
     	\hline 
	\end{tabular}
	\caption{Training and validation results of the model trained to detect Plasmodium parasites}
	\label{table:cnntrain}
\end{table}

Figure \ref{fig:cnnfalci2} shows the training accuracy per epoch of the CNN trained to classify species. Table \ref{table:cnntrain} shows the training results of the model. Positive detection was  simplified to classification to any from one of the three classes: vivax, falciparum gametocyte and falciparum ring-form. The model obtained a training accuracy of 94\% in detecting parasites on images and 87.6\% validation accuracy. 

Running on the test set, the model obtained an accuracy of 92.4\% in parasite detection (with 95.2\% sensitivity and 84.7\% specificity) and 87.9\% in species identification. Table \ref{table:cnnfalcieval} shows the confusion matrix of the CNN trained to identify species on the test set.

\begin{table}[!h]
	\centering
	\begin{tabular}{|p{0.5in}|p{0.5in}|p{0.5in}|p{0.5in}|p{0.5in}|}
	\hline  & VIVAX & FALCIPARUM GAMETOCYTE  & FALCIPARUM RING-FORM & NEGATIVE\\
	\hline VIVAX & 1654 & 46 & 92 & 80\\
	\hline FALCIPARUM GAMETOCYTE & 33 & 1214 & 1 & 24\\
	\hline FALCIPARUM RING-FORM & 111 & 11 & 1460 & 129\\
	\hline NEGATIVE & 88 & 26 & 161 & 1526\\
	\hline
	\end{tabular}
	\caption{Performance Evaluation on CNN trained to identify Plasmodium species}
	\label{table:cnnfalcieval}
\end{table}
\section{Conclusion and Recommendation}
This study has shown that malaria parasites can be effectively recognized on thin blood smears using a convolutional neural network. The features generated from the convolutional neural network are discriminant attributes for the detection of malaria parasites on images of thin blood smears, obtaining an accuracy of 92.4\%. Moreover, the study has also built a model using Inception v3 convolutional neural network to go beyond parasite detection to identify the species in the blood smear as either Plasmodium \textit{falciparum} or Plasmodium \textit{vivax} with an accuracy of 87.9\%.

As the dataset used is limited to only the trophozoite and gametocyte stages of the malaria parasites, future research may include images of the full schizogony of the parasites to build a more representative and discriminative model for species identification. 


%\section*{Acknowledgment}
\printbibliography
%\begin{thebibliography}{1}
%
%\bibitem{IEEEhowto:kopka}
%H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.
%
%\end{thebibliography}




% that's all folks
\end{document}


